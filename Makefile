IDIR =include
CC=gcc
CFLAGS=-I$(IDIR) -I/usr/include/cairo -Wall #-std=c11

SRCDIR=src
LDIR =../lib

LIBS=-lm -lcairo -lX11 -lXext

DEPS = comptoast.h

OBJ = toast_window.o toast.o main.o

BIN = comptoast

ifdef CT_DEBUG
	CFLAGS += -DCT_DEBUG -g
	OBJ += debug.o
endif

_DEPS = $(patsubst %,$(IDIR)/%,$(DEPS))
_OBJ = $(patsubst %,$(SRCDIR)/%,$(OBJ))

$(SRCDIR)/%.o: $(SRCDIR)/%.c $(_DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BIN): $(_OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

check-syntax:
	gcc -Wall -o nul -I$(IDIR) -S ${CHK_SOURCES}

.PHONY: clean

clean:
	rm -f $(SRCDIR)/*.o core $(BIN)


