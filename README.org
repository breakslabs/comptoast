* CompToast - a bone-simple "toast" daemon using XComposite and unix pipes

  CompToast is a X11 toast (also known as notifications, pop-ups, etc.)
  application that accepts text data over a unix pipe and displays it in a
  pretty bubble over-top of the display. It does not interface with dbus,
  systemd, any notification library, or the city of Atlantis. It is, in fact,
  very stupid and you probably should not ever use it for any reason, aside
  from possible educational purposes (e.g. this is what you don't do).

  The purpose of this code is to scratch a particular itch I have in the
  quickest, dirtiest way possible. Also, I haven't written any C code in a
  very long time (is it obvious?) so I thought it might be fun.

** Requirements
   
   - X11 (Xorg), natch
     - With XComposite extension and a compositor
   - a fairly recent version of cairo (I'm using 1.16.0)
   - an OS that handles Unix pipes (GNU/Linux, BSD, maybe OS X or macOS or
     something)
   - patience

** Building

   There's a Makefile that will probably work. There's no autoconf or anything
   clever like that. There is no install rule. Really - don't install this on
   your system. I have no idea what hazards it might contain. If you put this
   in /usr/local/bin and it summons an Elder God or raids your refrigerator, I
   am in no way responsible.

   Just try "make"

** Using

   You can't. It's not done yet. Thanks for playing.
