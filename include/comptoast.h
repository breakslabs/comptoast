#ifndef COMPTOAST_H
#define COMPTOAST_H

#include <stdint.h>

#define max(a,b)				\
  ({ __typeof__ (a) _a = (a);			\
    __typeof__ (b) _b = (b);			\
    _a > _b ? _a : _b; });

#define SLEEP_INTERVAL 50000

struct ct_conf {
  char *socket_path;
  char *font;
  unsigned int font_size;
  float alpha;
  float hold_time;
  float disolve_rate;
};

typedef struct ct_conf ct_conf;

#define INPUT_BUFFER_SIZE 64

#define LINE_QUEUE_LEN 16

int main(int argc, char **argv);
void main_loop(ct_conf *config, int pipe_fd);

#include "ct_debug.h"
#endif	/* COMPTOST_H  */
