#ifndef CT_DEBUG_H
#define CT_DEBUG_H

#ifndef COMPTOAST_H
#error "ct_debug.h should never be included directly - use CT_DEBUG env var"
#endif

#include <stdio.h>

void debug_conf(ct_conf *config);

#ifdef CT_DEBUG
   #define CT_DEBUG_PRINT_CONF(conf) debug_conf(conf);
#define CT_DBG(...) fprintf(stderr, "(%s [%s:%d]): ", __func__, __FILE__, \
			    __LINE__);                                    \
                    fprintf(stderr, __VA_ARGS__);                         
#else
   #define CT_DEBUG_PRINT_CONF(conf)
   #define CT_DBG(...)
#endif

#endif	/* CT_DEBUG_H */
