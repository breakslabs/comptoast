#ifndef CT_TOAST_H
#define CT_TOAST_H

#include <stdint.h>
#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xutil.h>

#include <cairo.h>
#include <cairo-xlib.h>

#include "comptoast.h"

#define PADDING 25
#define LINE_SPACING 10

/* The maximum number of lines of text allow in a single Toast */
#define TOAST_MAX_LINES 8

typedef struct Toast_s Toast;
typedef struct ToastWin_s ToastWin;
typedef struct XInfo_s XInfo;
typedef struct ToastText ToastText;

/* Retains X11-related data */
struct XInfo_s {
  Display *display;
  Window root;
  int default_screen;
  Window win;
  XVisualInfo vinfo;
  cairo_surface_t *surf;
  int screen_x;
  int screen_y;
  GC gc;
};

/* The Toast Window structure - this is not the same as the X window */
struct ToastWin_s {
  int x;
  int y;
  int width;
  int height;
  float alpha;
  long int hold_timer;
  long int fade_timer;
  long int move_timer;
  int target_x;
  int target_y;
  XInfo *xinfo;
  cairo_t *context;
  cairo_text_extents_t extents[TOAST_MAX_LINES];
  void (* reset)(ToastWin *win);
  void (* prepare)(ToastWin *win, ct_conf *conf, ToastText *text);
};

/* Structure representing a single Toast isntance */
struct Toast_s {
  unsigned char quad;
  ToastText *lines;
  ct_conf *config;
  ToastWin *window;
  void (* update)(Toast *self, uint64_t tdelta);
  void (* free)(Toast *self);
  void (* move_to)(Toast *self, int quad);
  int (* set_text)(Toast *self, ToastText* text);
  int (* ready)(Toast *self);
  /* const toast_vtable *vtable; */
};


struct ToastText {
  int pos;
  char *lines[TOAST_MAX_LINES];
  int (* full)(ToastText *self);
  void (* add_line)(ToastText *self, char *line);
  void (* free)(ToastText *self);
};

/* Toast Window */
ToastWin* init_toast_window(ct_conf *conf, XInfo *xinfo);

/* Toast */
Toast *init_toast(ct_conf *conf, XInfo *xinfo);
ToastText *init_toast_text(void);
XInfo *initialize_display(void);

#endif	/* CT_TOAST_H */
