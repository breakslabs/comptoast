#include <stdio.h>

#include "comptoast.h"

/* Pretty-print a ct_config instance to stderr */
void debug_conf(ct_conf *config) {
  fprintf(stderr, "Font: %s\n", config->font);
  fprintf(stderr, "Font-size: %d\n", config->font_size);
  fprintf(stderr, "Socket path: %s\n", config->socket_path);
  fprintf(stderr, "Alpha: %f\n", config->alpha);
  fprintf(stderr, "Hold time: %f (%fs)\n", config->hold_time,
	 config->hold_time/1000000);
  fprintf(stderr, "Disolve rate: %f (%fs)\n", config->disolve_rate,
	 config->disolve_rate/1000000);
  fprintf(stderr, "\n");
}  

