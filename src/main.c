#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "comptoast.h"
#include "toast.h"

#define CT_TMPDIRLEN 22 /* comptoast-XXXXXX\0 */
char _ct_tmpdir[CT_TMPDIRLEN];
char _ct_pipe[CT_TMPDIRLEN+5] = "";
int _running = 1;

static void usage(char *progname, char *optstrs, int show_long) {
  fprintf(stderr, "Usage: %s %s\n", progname, optstrs);
  if (!show_long)
    return;
  return;
}  

static void exit_handler(void) {
  if (_ct_pipe[0]) {
    unlink(_ct_pipe);
  }
  if (_ct_tmpdir[11] != 'X')
    rmdir(_ct_tmpdir);
}

static void signal_handler(int s) {
  _running = 0;
}

ct_conf *parse_cmdline(int argc, char **argv, ct_conf *config) {
  static struct option opts[] = {
				 {"font",         required_argument, 0, 'f'},
				 {"font-size",    required_argument, 0, 'z'},
				 {"alpha",        required_argument, 0, 'a'},
				 {"hold-time",    required_argument, 0, 'H'},
				 {"disolve-rate", required_argument, 0, 'D'},
				 {"socket-path",  required_argument, 0, 's'},
				 {"detach",       no_argument,       0, 'd'},
				 {"help",         no_argument,       0, 'h'},
				 {0,              0,                 0,  0}};
  const char *shortopts = "hdf:z:a:H:D:s:";
  char *progname = argv[0], *optstrs, *tstr;
  int ostrlen, tstrlen=0, opt_idx=0;

  for(int i=0; opts[i].name!=0; i++) {
    ostrlen += strlen(opts[i].name)+5;
    tstrlen = max(tstrlen, strlen(opts[i].name))
      }
  
  optstrs = (char *)malloc(ostrlen+1);
  tstr = (char *)malloc(tstrlen+6);
  optstrs[0] = (char)0;
  
  for(int i=0; opts[i].name!=0; i++) {
    sprintf(tstr, " --%s/-%c", opts[i].name, opts[i].val);
    strcat(optstrs, tstr);
  }

  char c;
  while (1) { // TODO: Error handling, pleaz
    c = getopt_long(argc, argv, shortopts, opts, &opt_idx);

    if (c == -1)
      break;
    
    switch(c) {
    case 'h':
      usage(progname, optstrs, 1);
      exit(0);
    case 'f':
      config->font = optarg;
      break;
    case 'z':
      config->font_size = atoi(optarg);
      break;
    case 's':
      config->socket_path = optarg;
      break;
    case 'a':
      config->alpha = atof(optarg);
      break;
    case 'H':
      config->hold_time = atof(optarg)*1000000;
      break;
    case 'D':
      config->disolve_rate = atof(optarg)*1000000;
      break;
    case 'd':
      fprintf(stderr, "Daemon mode not implemented\n");
      exit(1);
    case '?':
      printf("Got '?'\n");
      break;
    default:
      printf("DEFAULT\n");
    }
  }
  return config;
}


int initialize_pipe(ct_conf *conf) {
  int fd;
  
  sprintf(_ct_tmpdir, "/tmp/comptoast-XXXXXX");
  if (!mkdtemp(_ct_tmpdir)) {
    fprintf(stderr, "could not create tmpdir '%s': ", _ct_tmpdir);
    perror(NULL);
    exit(1);
  }
  CT_DBG("tmpdir is '%s'\n", _ct_tmpdir);
  
  sprintf(_ct_pipe, "%s/fifo", _ct_tmpdir);
  /* The following is undefined in POSIX. Under Linux, opening a pipe R/W */
  /* prevents the read end of the pipe from closing on EOF. We should */
  /* probably hadle this differently if we intend to ever be portable.*/
  if (mkfifo(_ct_pipe, 0600)) {
    fprintf(stderr, "Could not create named pipe '%s': ", _ct_pipe);
    perror(NULL);
    exit(1);
  }
  CT_DBG("Pipe is '%s'\n", _ct_tmpdir);
  
  if ((fd = open(_ct_pipe, O_RDWR | O_NONBLOCK)) < 0) {
    fprintf(stderr, "Could not open named pipe '%s': ", _ct_pipe);
    perror(NULL);
  }
  return fd;
}

/* Read
 *
 */
int pipe_read_into(int fd, char *buf) { /* NB - do mallopt() */
  static char ibuf[INPUT_BUFFER_SIZE];
  static int cpos = 0;
  int i, got_line = 0;

  int count = read(fd, ibuf+cpos, INPUT_BUFFER_SIZE-cpos);
  if (count < 0)
    count = 0;
  /* FIXME N.B. needs to handle multple newlines in sequence */
  for (i=0; i<count+cpos; i++)
    if (ibuf[i] == '\n')
      break;
  
  if (i < (cpos+count)) { /* Found a newline */
    strncpy(buf, ibuf, i);
    buf[i] = 0;
    cpos = (cpos + count) - i - 1;
    if (cpos > 0)
      memmove(ibuf, ibuf+i+1, cpos);
    got_line = 1;
  } else if (i >= INPUT_BUFFER_SIZE) { /* Buffer's full - return it as is */
    strncpy(buf ,ibuf, INPUT_BUFFER_SIZE);
    cpos = 0;
    got_line = 1;
  } else {
    cpos += count;
  }
  if (cpos > 0)
    got_line *= -1;
  return got_line;
}
      
    
int main(int argc, char **argv) {
  int pipe_fd;
  ct_conf config = { NULL, "Courier", 30, 0.8, 3000000, 500000 };
  /* double braces below silences GCC bug 53119 w/ -Wall/-Wmissing-braces */
  struct sigaction sa = {{ signal_handler }};
    
  memset(_ct_tmpdir, 0, CT_TMPDIRLEN);
  parse_cmdline(argc, argv, &config);
  CT_DEBUG_PRINT_CONF(&config);

  pipe_fd = initialize_pipe(&config);
  
  atexit(exit_handler);
  if (-1 == sigaction(SIGINT, &sa, NULL)) {
    perror("Could not install signal handler for SIGINT");
    exit(EXIT_FAILURE);
  }
  if (-1 == sigaction(SIGTERM, &sa, NULL)) {
    perror("Could not install signal handler for SIGTERM");
    exit(EXIT_FAILURE);
  }

  main_loop(&config, pipe_fd);
  close(pipe_fd);
  return 0;
}

uint64_t mt_clock(void) {
  struct timespec ts;

  if (clock_gettime (CLOCK_MONOTONIC, &ts) == 0)
    return (uint64_t)(ts.tv_sec * 1000000 + ts.tv_nsec / 1000);
  else
    return 0;
}

uint64_t std_clock(void) {
  struct timeval tv;
  
  if (gettimeofday(&tv, NULL) == 0)
    return (uint64_t)(tv.tv_sec * 1000000 + tv.tv_usec);
  else
    return 0;
}

void *get_timer(void) {
#ifdef _POSIX_MONOTONIC_CLOCK
#ifdef _SC_MONOTONIC_CLOCK
  if (sysconf(_SC_MONOTONIC_CLOCK) > 0) {
    CT_DBG("Found monotonic clock\n");
    return mt_clock;
  }
#endif
#endif
  CT_DBG("Using \"gettimeofday\" for timing\n");
  return std_clock;
}

void main_loop(ct_conf *config, int pipe_fd) {
  int tdelta=0, last_frame, line_ready=0;
  char *buf = NULL;
  uint64_t (*timer)(void);
  XInfo *xinfo = initialize_display();
  Toast *toast = init_toast(config, xinfo);
  ToastText *queue = init_toast_text();
  nfds_t nfds = 1;
  struct pollfd pfd[1];

  pfd[0].fd = pipe_fd;
  pfd[0].events = POLLIN;


  timer = get_timer();
  last_frame = timer();
  
  while(_running) {
    int preslt;
    
    if ((preslt = poll(pfd, nfds, 0)) < 0) 
      perror(NULL);

    if (((preslt > 0) || (line_ready < 0)) && !queue->full(queue)) {
      if (!buf)
	buf = (char *)malloc(INPUT_BUFFER_SIZE);
      line_ready = pipe_read_into(pipe_fd, buf);
    } else
      line_ready = 0;
    
    if(line_ready || queue->full(queue)) {
      CT_DBG("Got line '%s' [%d]\n", buf, line_ready);
      if (queue->full(queue) || (buf[0]=='.' && buf[1]==0)) {
	CT_DBG("Got message\n");
	if (toast->ready(toast)) {
	  toast->set_text(toast, queue);
	  if (buf)
	    buf[0] = '\0';
	}
	queue = init_toast_text();
      } else {
	queue->add_line(queue, buf);
	buf = (char *)NULL;
      }
    }
      
    toast->update(toast, tdelta);
    usleep(SLEEP_INTERVAL);
    tdelta = timer() - last_frame;
    last_frame = timer();
    //CT_DBG("%d ms elapsed\n", tdelta);
  }
}
