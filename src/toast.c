#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include <cairo.h>
#include <cairo-xlib.h>

#include "toast.h"
#include "comptoast.h"

/* Check if a ToastText buffer is full
 *
 * return - 1 if full, else 0
 */
int toast_text_full(ToastText *text) {
  return text->pos >= TOAST_MAX_LINES;
}

/* Add a line of text to a ToastText buffer
 *
 * param line - a null-terminated string
 */
void toast_text_add_line(ToastText *text, char *line) {
  text->lines[text->pos] = line;
  text->pos += 1;
}

/*  Free a ToastText instance and all references strings
 */
void toast_text_free(ToastText *text) {
  for (int i=0; i<text->pos; i++)
    free(text->lines[i]);
  free(text);
}

/* Initialize a ToastText instance
 *
 * return - an initialized ToastText instance
 */
ToastText *init_toast_text(void) {
  ToastText *text = malloc(sizeof(ToastText));
  text->pos = 0;
  text->free = toast_text_free;
  text->full = toast_text_full;
  text->add_line = toast_text_add_line;
  return text;
}

/* XMoveWindow / XResizeWindow */
/* XCopyRectangle, XCopyArea, XCreatePixmap */
/* FIXME: accept display string here? */
/* Initialize X11 connection and window.
 * Allocates an XInfo structure and populates and returns it, relinquishing
 * ownership.
 */
XInfo *initialize_display(void) {
  XInfo *xinfo = malloc(sizeof(XInfo));
  xinfo->display = XOpenDisplay(NULL);
  xinfo->root = DefaultRootWindow(xinfo->display);
  xinfo->default_screen = XDefaultScreen(xinfo->display);

  Screen *screen = XDefaultScreenOfDisplay(xinfo->display);
  xinfo->screen_x = screen->width;
  xinfo->screen_y = screen->height;
  
  /* Allow pass-through? */
  XSetWindowAttributes attrs;
  attrs.override_redirect = True;
  
  if (!XMatchVisualInfo(xinfo->display,
			/* DefaultScreen(xinfo->display), */
			xinfo->default_screen,
			32, TrueColor, &(xinfo->vinfo))) {
    printf("No visual found supporting 32 bit color, terminating\n");
    exit(1);
  }

  int shape_event_base, shape_error_base;
  if (!XShapeQueryExtension(xinfo->display,
			    &shape_event_base, &shape_error_base)) {
    fprintf(stderr, "Shape extension not available\n");
    exit(1);
    }

  /* these next three lines add 32 bit depth */
  /* remove if you dont need and change the flags below */
  attrs.colormap = XCreateColormap(xinfo->display, xinfo->root,
				   xinfo->vinfo.visual, AllocNone);
  attrs.background_pixel = 0;
  attrs.border_pixel = 0;

  xinfo->win =
    XCreateWindow(xinfo->display, xinfo->root,
		  0, 0, 1 /*w*/, 1 /*h*/, 0,
		  xinfo->vinfo.depth, InputOutput, 
		  xinfo->vinfo.visual,
		  CWOverrideRedirect | CWColormap | CWBackPixel | CWBorderPixel,
		  &attrs);
  xinfo->gc = XCreateGC(xinfo->display, xinfo->win, 0, 0);
  xinfo->surf = cairo_xlib_surface_create(xinfo->display, xinfo->win,
					  xinfo->vinfo.visual,
					  300, 150);
  /* Make input window mask 0x0 using Shape extention */
  Region region = XCreateRegion();
  XRectangle rect = { 0, 0, 0, 0 };
  XUnionRectWithRegion(&rect, region, region);
  XShapeCombineRegion(xinfo->display,xinfo->win,
		      ShapeInput, 0, 0, region, ShapeSet);
  XDestroyRegion(region);

  XMapWindow(xinfo->display, xinfo->win);
  return xinfo;
}

/* Draw the toast to the window */
void draw_toast(Toast * toast) {
  double x         = 0.0,        /* parameters like cairo_rectangle */
         y         = 0.0,
         width         = (float)toast->window->width-4,
         height        = (float)toast->window->height-4,
         aspect        = 1.0,     /* aspect ratio */
         corner_radius = height / 3.0;   /* and corner curvature radius */

  double radius = corner_radius / aspect;
  double degrees = M_PI / 180.0;
  cairo_t *cr = toast->window->context;
  double alpha = (toast->window->fade_timer / toast->config->disolve_rate);
  alpha *= toast->config->alpha;

  /* Set surface to translucent color (r, g, b, a) without disturbing graphics state. */
  /* Probably this is better than the XFillRectangle thing. Maybe
   * cairo_save (cr);
   * cairo_set_source_rgba (cr, r, g, b, a);
   * cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
   * cairo_paint (cr);
   * cairo_restore (cr); */
  
  XSetForeground(toast->window->xinfo->display, toast->window->xinfo->gc,
  		 0x0);
  XSetBackground(toast->window->xinfo->display, toast->window->xinfo->gc,
  		 0x0);
  XFillRectangle(toast->window->xinfo->display,
  		 toast->window->xinfo->win,
  		 toast->window->xinfo->gc,
  		 0, 0,
  		 toast->window->width, toast->window->height);
  
  cairo_push_group(cr);
  cairo_new_sub_path(cr);
  cairo_arc(cr, x + width - radius, y + radius,
	    radius, -90 * degrees, 0 * degrees);
  cairo_arc(cr, x + width - radius, y + height - radius,
	    radius, 0 * degrees, 90 * degrees);
  cairo_arc(cr, x + radius, y + height - radius, radius,
	    90 * degrees, 180 * degrees);
  cairo_arc(cr, x + radius, y + radius, radius, 180 * degrees, 270 * degrees);
  cairo_close_path(cr);
  
  cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, alpha);
  cairo_fill_preserve(cr);
  cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, alpha);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  cairo_select_font_face(cr, toast->config->font, CAIRO_FONT_SLANT_NORMAL,
			 CAIRO_FONT_WEIGHT_BOLD);
  cairo_set_font_size(cr, toast->config->font_size);
  cairo_set_source_rgba(cr, 0.2, 0.2, 0.9, alpha);
  
  int h=PADDING, hw=toast->window->width/2;
  for (int i = 0; i < toast->lines->pos; i++) {
    int w;
    w = hw - toast->window->extents[i].width/2;
    h += toast->window->extents[i].height;
    cairo_move_to(cr, w, h);
    cairo_text_path(cr, toast->lines->lines[i]);
    h += LINE_SPACING;
    //h += (toast->window->extents[i].height/2) + LINE_SPACING;
  }
  cairo_clip(cr);
  cairo_paint_with_alpha(cr, alpha);
  cairo_pop_group_to_source(cr);
  cairo_paint_with_alpha(cr, alpha);
  cairo_surface_flush(toast->window->xinfo->surf);
  XFlush(toast->window->xinfo->display);
}
  

/* Update the Toast 
 * Advances hold timers, fade timers, and move timers as appropriate, then
 * calls draw().
 *
 * param tdelta - Time delta in microseconds. Typically this value should be
 *     the number of microseconds that have passed since the last time
 *     update() was called.
 */
void toast_update(Toast *toast, uint64_t tdelta) {
  if ((!toast->window->context) || (!toast->window->xinfo->surf))
    return;
  if (toast->window->hold_timer > 0) {
    //CT_DBG("HOLD %ld\n", toast->window->hold_timer);
    toast->window->hold_timer -= tdelta;
  } else if (toast->window->fade_timer > 0) {
    toast->window->fade_timer -= tdelta;
  } else {
    toast->window->reset(toast->window);
    return;
  }
  XMoveWindow(toast->window->xinfo->display, toast->window->xinfo->win,
	      toast->window->xinfo->screen_x - toast->window->width,
	      toast->window->xinfo->screen_y - toast->window->height);
  draw_toast(toast);
}

/* De-allocate this Toast.
 * Frees the memory occupied by this Toast. All internal data is freed,
 * including any existing 'lines[]'. Toast.config and Toast.Window.XInfo
 * structures are *not* de-allocated.
*/
void toast_free(Toast *toast) {
  /* FIXME - free cairo stuff */
  if (toast->lines)
    toast->lines->free(toast->lines);
  free(toast);
}

/* Set the quadrant target for toast to 'quad' 
 * Uses Cartesian quadrants (1 is upper right, 2 is upper left, 3 is lower
 * left, 4 is lower right). For fun, 0 is center screen. This just sets the
 * target - calls to 'update' do the actual moving.
 *
 * param int - quadrant value. Valid values are 0 <= 'quad' <= 4
 */
void toast_move_to(Toast *toast, int quad) {
  int tx, ty;
  XWindowAttributes wattr;
  
  if ((quad<0) || (quad>4))
    return ;
  toast->quad = quad;
  if (!toast->window)
    return ;
  switch(quad) {
  case 0:
    tx = (toast->window->xinfo->screen_x/2) - (toast->window->width/2);
    ty = (toast->window->xinfo->screen_y/2) - (toast->window->height/2);
    break;
  case 1: 
    tx = toast->window->xinfo->screen_x - toast->window->width;
    ty = 0;
    break;
  case 2:
    tx = 0;
    ty = 0;
    break;
  case 3:
    tx = 0;
    ty = toast->window->xinfo->screen_y - toast->window->height;
    break;
  case 4:
    tx = toast->window->xinfo->screen_x - toast->window->width;
    ty = toast->window->xinfo->screen_y - toast->window->height;
    break;
  }
  /* Shouldn't need to call XTranslateCoordinates since parent is root? */
  XGetWindowAttributes(toast->window->xinfo->display,
		       toast->window->xinfo->win, &wattr);
  CT_DBG("Toast was at (%d, %d) when toast_move_to() was called\n",
	 wattr.x, wattr.y);
  toast->window->x = wattr.x;
  toast->window->y = wattr.y;
  toast->window->target_x = tx;
  toast->window->target_y = ty;
}

/* Set the text on this Toast instance. 
 * Should not be called without checking toast->is_ready() first. Number of
 * lines may not exceed TOAST_TOTAL_LINES. This takes ownership of lines and
 * the pointers therein - don't send stack data or things you want to keep.
 * 
 * param text - pointer to a ToastText instance
 *
 * Returns 0 on success, a positive integer on failure.
 */
int toast_set_text(Toast *toast, ToastText *text) {
  if (! toast->ready(toast))
    return 1;
  CT_DBG("Setting toast text...[%d]\n", text->pos);
  if (toast->lines)
    toast->lines->free(toast->lines);
  toast->lines = text;
  toast->window->prepare(toast->window, toast->config, text);
  return 0;
}

/* Return True if toast is ready to accept new text, else False */
int toast_ready(Toast *toast) {
  return !(toast->window->hold_timer > 0);
}

/* Initialize a Toast instance.
 * Allocates a Toast structure and prepares it for use prior to returning
 * it. Ownership of Toast is relinquished.
 * 
 * param conf - pointer to a populated ct_config structure. A reference to the
 *     pointer is kept for the lifetime of the Toast, but is not managed.
 * param xinfo - pointer to a populated XInfo strcture. Like 'conf', a
 *     reference is retained for the lifetime of the Toast, but not freed when
 *     the Toast is destroyed.
 *
 * returns - a pointer to a Toast instance.
 */
Toast *init_toast(ct_conf *conf, XInfo *xinfo) {
  Toast *toast = malloc(sizeof(Toast));
  /* What if it didn't malloc? */
  toast->quad = 4; /* Cartesian - lower right corner */
  toast->config = conf;
  
  toast->update = toast_update;
  toast->free = toast_free;
  toast->move_to = toast_move_to;
  toast->set_text = toast_set_text;
  toast->ready = toast_ready;
  toast->window = init_toast_window(conf, xinfo);
  toast->move_to(toast, 2);
  toast->window->reset(toast->window);
  return toast;
}
