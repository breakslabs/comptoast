#include <stdlib.h>

#include <cairo.h>
#include <cairo-xlib.h>

#include "toast.h"


/* Reset the ToastWin to prepare it to be used again later.
 * Clears any existing ToastWin display and data. Resets any
 * running timers and completes movement if Toast was travelling. Resizes X
 * window to 1x1. Destroys Cairo data.
 */
void toast_window_reset(ToastWin *win) {
  win->hold_timer = 0;
  win->fade_timer = 0;
  win->move_timer = 0;
  win->x = win->target_x;
  win->y = win->target_y;
  if ((win->width > 0) || (win->height > 0)) {
    win->width = 0;
    win->height = 0;
    XResizeWindow(win->xinfo->display,
		  win->xinfo->win, 1, 1);
  }

}

/* Prepare a ToastWin for drawing.
 *
 * Resets the ToastWin and generates a new CairoContext
 */
void toast_window_prepare(ToastWin *win, ct_conf *conf, ToastText *text) {
  
  /* win->xinfo->surf = cairo_xlib_surface_create(win->xinfo->display,
   * 					       win->xinfo->win,
   * 					       win->xinfo->vinfo.visual,
   * 					       win->width,
   * 					       win->height); */
  /* win->context = cairo_create(win->xinfo->surf); */
  cairo_select_font_face(win->context,
			 conf->font, CAIRO_FONT_SLANT_NORMAL,
			 CAIRO_FONT_WEIGHT_BOLD);
  cairo_set_font_size(win->context, conf->font_size);
  for (int i=0; i<text->pos; i++) {
    cairo_text_extents(win->context, text->lines[i],
		       &(win->extents[i]));
    if(win->extents[i].width > win->width)
      win->width = win->extents[i].width;
    win->height += win->extents[i].height + LINE_SPACING;
  }
  win->height += (PADDING*2);
  win->width += (PADDING*2);
  XResizeWindow(win->xinfo->display,
		win->xinfo->win,
		win->width, win->height);
  cairo_xlib_surface_set_size(win->xinfo->surf, win->width, win->height);
  win->hold_timer = conf->hold_time;
  win->fade_timer = conf->disolve_rate;
}

/* Creates and populates ToastWin structure */
ToastWin* init_toast_window(ct_conf *conf, XInfo *xinfo) {
  ToastWin *win = calloc(1, sizeof(ToastWin));
  win->xinfo = xinfo;
  win->context = cairo_create(win->xinfo->surf);
  win->prepare = toast_window_prepare;
  win->reset = toast_window_reset;
  return win;
}
